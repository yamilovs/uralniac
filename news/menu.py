#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import NewsItem

class NewsMenu(CMSAttachMenu):
    name = u'Меню новостей'

    def get_nodes(self, request):
        nodes = []

        for item in NewsItem.objects.all():
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        item.pk,
                                        visible=False))
        return nodes

menu_pool.register_menu(NewsMenu) # register the menu