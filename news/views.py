#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *
from uralniac.views import SetMeta

class NewsItemListView(ListView):
    model = NewsItem
    context_object_name='items'
    template_name = 'news/list.html'

    def get_queryset(self):
        return super(NewsItemListView, self).get_queryset().order_by('-created')


class NewsItemDetailView(DetailView, SetMeta):
    model = NewsItem
    context_object_name='item'
    template_name = 'news/item.html'

    def get_template_names(self):
        names = super(NewsItemDetailView, self).get_template_names()

        if self.request.is_ajax():
            return ['news/ajax_item.html']

        return names



class AjaxNewsItemDetailView(DetailView, SetMeta):
    model = NewsItem
    context_object_name='item'
    template_name = 'news/ajax_item.html'