#-*- coding: UTF-8 -*-


from datetime import datetime

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from uralniac.models import MetaFields

class NewsItem(MetaFields):
    title = models.CharField(u'Заголовок новости', max_length=150)

    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="news_img")

    content = HTMLField(u'Содержимое', blank=True, configuration='CKEDITOR_HTML_SETTINGS')

    created = models.DateTimeField(u'Дата добавления', auto_now=False,
           auto_now_add=True, null=True, blank = False, default = datetime.now)

    updated = models.DateTimeField(u'Дата изменения', auto_now=True,
           auto_now_add=True, blank = False, default = datetime.now)

    @models.permalink
    def get_absolute_url(self):
        return 'news_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'
