# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150129_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsitem',
            name='title',
            field=models.CharField(max_length=150, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043d\u043e\u0432\u043e\u0441\u0442\u0438'),
            preserve_default=True,
        ),
    ]
