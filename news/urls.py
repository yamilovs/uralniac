from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', NewsItemListView.as_view()),

    url(r'^(?P<pk>\d+)/$', NewsItemDetailView.as_view(),
        name='news_item'),
)
