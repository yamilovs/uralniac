from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', ArticleItemListView.as_view()),

    url(r'^(?P<pk>\d+)/$', ArticleItemDetailView.as_view(),
        name='article_item')
)
