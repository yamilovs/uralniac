#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class ArticleItemAdmin(admin.ModelAdmin):
    ordering = ['order',]
    list_display = ('title', 'order',)

admin.site.register(ArticleItem, ArticleItemAdmin)