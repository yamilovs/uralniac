from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.sitemaps import GenericSitemap
from cms.sitemaps import CMSSitemap
from article.models import ArticleItem
from directions.models import DirectionsItem, SolutionItem
from news.models import NewsItem
from project.models import Project
from employee.models import Employee

article_dict = {
    'queryset': ArticleItem.objects.all(),
}
directions_dict = {
    'queryset': DirectionsItem.objects.all(),
}
solution = {
    'queryset': SolutionItem.objects.all(),
}
news_dict = {
    'queryset': NewsItem.objects.all(),
    'date_field': 'created'
}
project_dict = {
    'queryset': Project.objects.all(),
}

employee_dict = {
    'queryset': Employee.objects.all(),
}

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'uralniac.views.page', name='page'),
    # url(r'^(?P<page>\w+)/$', 'uralniac.views.page', name='page'),

    # url(r'^$', 'uralniac.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {
        'cmspages': CMSSitemap,
        'article': GenericSitemap(article_dict, priority=0.4),
        'directions': GenericSitemap(directions_dict, priority=0.4),
        'solution': GenericSitemap(directions_dict, priority=0.4),
        'news': GenericSitemap(news_dict, priority=0.4),
        'project': GenericSitemap(project_dict, priority=0.4),
        'employee': GenericSitemap(employee_dict, priority=0.4),
    }}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^filebrowser_filer/', include('ckeditor_filebrowser_filer.urls')),
    url(r'^feedback_form/', 'uralniac.views.feedback_form', name='feedback_form'),
    url(r'^', include('cms.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                {'document_root': settings.MEDIA_ROOT }
        )
    )