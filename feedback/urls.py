from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', FeedbackItemListView.as_view()),

    url(r'^(?P<pk>\d+)/$', FeedbackItemDetailView.as_view(),
        name='feedback_item')
)
