#-*- coding: UTF-8 -*-


from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from uralniac.models import MetaFields

class FeedbackItem(MetaFields):
    title = models.CharField(u'Название отзыва', max_length=100)

    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="feedback_img")

    file =  FilerFileField(verbose_name=u'Скан с отзывом', null=True, blank=True,
                           related_name="feedback_file")

    description = HTMLField(u'Содержимое отзыва', blank=True, max_length=2000)

    copyright = models.CharField(u'Подпись', blank=True, max_length=255)

    order = models.IntegerField(u'Подрядок сортировки', default=0, blank=True)


    @models.permalink
    def get_absolute_url(self):
        return 'feedback_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'
