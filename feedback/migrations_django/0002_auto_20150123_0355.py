# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackitem',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(max_length=255, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u043e\u0442\u0437\u044b\u0432\u0430', blank=True),
            preserve_default=True,
        ),
    ]
