#-*- coding: UTF-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import FeedbackMenu

class FeedbackApphook(CMSApp):
    name = u'Отзывы'
    urls = ['feedback.urls']
    menus = [FeedbackMenu]

apphook_pool.register(FeedbackApphook)