# -*- coding: UTF-8 -*-
from django.views.generic.list import ListView

from .models import *


class EmployeeListView(ListView):
    model = Employee
    context_object_name = 'items'
    template_name = 'employee/list.html'

    def get_queryset(self):
        return super(EmployeeListView, self).get_queryset().order_by('-created')
