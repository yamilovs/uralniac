# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField


class Employee(models.Model):
    first_name = models.CharField(u'Имя', max_length=100)
    last_name = models.CharField(u'Фамилия', max_length=100)
    patronymic = models.CharField(u'Отчество', max_length=100)
    position = models.CharField(u'Должность', max_length=255)
    photo = FilerImageField(null=True, blank=True, related_name="employee_photo")
    description = HTMLField(u'Описание', blank=True, configuration='CKEDITOR_HTML_SETTINGS')
    created = models.DateTimeField(u'Дата добавления', auto_now=False, auto_now_add=True, null=True, blank=False, default=datetime.now)
    updated = models.DateTimeField(u'Дата изменения', auto_now=True, auto_now_add=True, blank=False, default=datetime.now)

    def __unicode__(self):
        return " ".join([self.first_name, self.last_name, self.patronymic])

    class Meta:
        verbose_name = u'Сотрудник'
        verbose_name_plural = u'Сотрудники'
