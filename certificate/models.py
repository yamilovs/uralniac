#-*- coding: UTF-8 -*-

from django.db import models
from filer.fields.image import FilerImageField

class CertificateItem(models.Model):
    title = models.CharField(u'Название сертификата', max_length=100)

    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="cert_img")

    @models.permalink
    def get_absolute_url(self):
        return 'certificate_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Сертификат'
        verbose_name_plural = u'Сертификаты'
