#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class CertificateItemAdmin(admin.ModelAdmin):
    ordering = ['id',]
    list_display = ('title',)

admin.site.register(CertificateItem, CertificateItemAdmin)