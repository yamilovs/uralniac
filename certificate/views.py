#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *

class CertificateItemListView(ListView):
    model = CertificateItem
    context_object_name='items'
    template_name = 'certificate/list.html'

    def get_queryset(self):
        return super(CertificateItemListView, self).get_queryset()


class CertificateItemDetailView(DetailView):
    model = CertificateItem
    context_object_name='item'
    template_name = 'certificate/item.html'