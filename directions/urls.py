from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', DirectionsItemListView.as_view()),

    url(r'^direction/(?P<pk>\d+)/$', DirectionsItemDetailView.as_view(),
        name='directions_item'),
)
