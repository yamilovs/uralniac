#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *
from uralniac.views import SetMeta


class DirectionsItemListView(ListView):
    model = DirectionsItem
    context_object_name='items'
    template_name = 'directions/list.html'

    def get_queryset(self):
        return super(DirectionsItemListView, self).get_queryset().order_by('order')


class DirectionsItemDetailView(DetailView, SetMeta):
    model = DirectionsItem
    context_object_name='item'
    template_name = 'directions/item.html'



class SolutionItemListView(ListView):
    model = SolutionItem
    context_object_name='items'
    template_name = 'directions/solution-list.html'

    def get_queryset(self):
        return super(SolutionItemListView, self).get_queryset().order_by('order')


class SolutionItemDetailView(DetailView, SetMeta):
    model = SolutionItem
    context_object_name='item'
    template_name = 'directions/solution-item.html'