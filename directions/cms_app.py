#-*- coding: UTF-8 -*-
from django.conf.urls import patterns, url

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import DirectionsMenu, SolutionMenu
from .views import *

class DirectionsApphook(CMSApp):
    name = u'Направления деятельности'
    urls = ['directions.urls']
    menus = [DirectionsMenu]
apphook_pool.register(DirectionsApphook)


class SolutionApphook(CMSApp):
    name = u'Решения'
    urls = [
        patterns('solution',
            url(r'^$', SolutionItemListView.as_view()),
            url(r'^solution/(?P<pk>\d+)/$', SolutionItemDetailView.as_view(),
                name='solution_item')
        )
    ]
    menus = [SolutionMenu]
apphook_pool.register(SolutionApphook)