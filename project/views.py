#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *
from uralniac.views import SetMeta

class ProjectListView(ListView):
    model = Project
    context_object_name='items'
    template_name = 'project/list.html'

    def get_queryset(self):
        return super(ProjectListView, self).get_queryset().order_by('-id')


class ProjectItemDetailView(DetailView, SetMeta):
    model = Project
    context_object_name='item'
    template_name = 'project/item.html'