#-*- coding: UTF-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import ProjectMenu

class ProjectApphook(CMSApp):
    name = u'Проекты'
    urls = ['project.urls']
    menus = [ProjectMenu]

apphook_pool.register(ProjectApphook)