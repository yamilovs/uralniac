#-*- coding: UTF-8 -*-
from django.contrib import admin
from .models import Project

class ProjectItemAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Project, ProjectItemAdmin)